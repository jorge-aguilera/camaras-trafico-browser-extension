
let baseM30Url = "http://www.mc30.es/components/com_hotspots/datos/imagenes_camaras/";
let baseMadrid = "http://informo.munimadrid.es/cameras/Camara";

let camaras = Array.from([
  {name:'',num:-1,url:baseM30Url+"M30-PK00+000C(MANOTERAS).jpg"},
  {name:'',num:-2,url:baseM30Url+"M30-PK00+900C.jpg"},
  {name:'',num:-3,url:baseM30Url+"M30-PK01+500C(PIOXII).jpg"},
  {name:'',num:-4,url:baseM30Url+"M30-PK02+900X(COSTARICA).jpg"},
  {name:'',num:-5,url:baseM30Url+"M30-PK04+700D(AVAMERICA).jpg"},
  {name:'',num:-6,url:baseM30Url+"M30-PK06+000D(ALCALA).jpg"},
  {name:'',num:-7,url:baseM30Url+"M30-PK07+600D(ODONNELL).jpg"},
  {name:'',num:-8,url:baseM30Url+"M30-PK08+000D(DAI).jpg"},
  {name:'',num:-9,url:baseM30Url+"M30-PK09+100CDAIAVMEDIT..jpg"},
  {name:'',num:-10,url:baseM30Url+"M30-PK09+800D(A3).jpg"},
  {name:'',num:-11,url:baseM30Url+"M30-PK10+000C.jpg"},
  {name:'',num:-12,url:baseM30Url+"M30-PK10+900C(M.ALVARO).jpg"},
  {name:'',num:-13,url:baseM30Url+"M30-PK12+500C(NUDOSUR).jpg"},
  {name:'',num:-14,url:baseM30Url+"M30-PK12+500C(NUDOSUR-A4).jpg"},
  {name:'',num:-15,url:baseM30Url+"M30-PK16+000C(CALDERON).jpg"},
  {name:'',num:-16,url:baseM30Url+"M30-PK19+700D(MONISTROL).jpg"},
  {name:'',num:-17,url:baseM30Url+"M30-PK20+700D(P.FRANCESES).jpg"},
  {name:'',num:-18,url:baseM30Url+"M30-PK22+400D(VETERINARIA).jpg"},
  {name:'',num:-19,url:baseM30Url+"M30-PK24+800D(ARR.FRESNO).jpg"},
  {name:'',num:-20,url:baseM30Url+"M30-PK25+000C(NUDOM-40).jpg"},
  {name:'',num:-21,url:baseM30Url+"M30-PK29+800C(R.ACADEM.).jpg"},
  {name:'',num:-22,url:baseM30Url+"M30-PK30+500C(M607).jpg"},
  {name:'',num:-23,url:baseM30Url+"M30-PK31+700C.jpg"},
]);

function prepareCameras( callback ){
  var x = new XMLHttpRequest();
  x.open("GET", "http://informo.munimadrid.es/informo/tmadrid/CCTV.kml", true);
  x.onreadystatechange = function () {
    if (x.readyState == 4 && x.status == 200)
    {      
      let document = x.responseXML.documentElement;      
      let placemarks = document.getElementsByTagName("Placemark")
      for( var i in placemarks){
        let pm = placemarks[i];        
        if( !pm.getElementsByTagName)continue;
        let extData = pm.getElementsByTagName("ExtendedData")[0].getElementsByTagName("Data");        
        let num = extData[0].getElementsByTagName("Value")[0].childNodes[0].nodeValue;      
        let name = extData[1].getElementsByTagName("Value")[0].childNodes[0].nodeValue;
        camaras.push({
          name: name,
          num: num,
          url : "http://informo.munimadrid.es/cameras/Camara"+num+".jpg"
        })        
      }
      camaras = camaras.sort(function(a,b){return a.name.localeCompare(b.name)})
      callback(camaras);
    }
  };
  x.send(null);
}